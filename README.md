# Application of Domain.com Coupons in Hackathons

Established in 2013, Major League Hacking (MLH), is an organization which organizes a league for student hackathons. It was formed by two former developer evangelists, Mike Swift and Jonathan Gottfield, who previously worked for SendGrid and Twilio respectively.

For the last two years, Domain.com has been [providing special offers](https://www.webhostingology.com/domain-com-coupon-codes/) to hackers who participate in MLH events. Using these coupons, together with several other promotional codes and vouchers, enables students at each event to register a free domain (.com, .net or .org) and get discounts on hosting charges. In addition, a bag full of great hacker gear is given to the most innovative domain name at each event.

##But what exactly is MLH?

MLH is an involved and spirited maker community for the upcoming generation of technology leaders and entrepreneurs. 

The original intention of MLH was to create a forum through which North American student-run University hackathons could be organized. MLH organizes more than 200 of these weekend-long events each year with the aim of inspiring innovation, growing communities and imparting crucial computer skills to more than 65,000 students from around the world. 

##What is a hackathon?

Coined from the joining of two words, hack and marathon, a hackathon refers to an event in which computer programmers and other software development experts such as project managers, graphic designers, interface designers, and others like subject-matter experts, congregate and participate in the design sprint-like event in a vigorous software project. Occasionally, a hackathon may have a hardware aspect.

Although the word hackathon is derived from the word hack, which refers to an unauthorized access to computer systems, it is strictly used to refer to exploratory programming and has no connecting with any form of hacking. A hackathon is also referred to as a hack day, hackfest or codefest.

##How long do the events last

Ordinarily, the length of a hackathon ranges from one day to one week and its focus could be educational or social, although sometimes hackathons are arranged with the aim of developing a usable software.

From the mid to the late 2000s, hackathons became very popular and attracted the interest of companies and venture capitalists. This was mainly because the hackathons were noted to have the capacity for rapid development of software and technologies while coders get together to [edit code in the cloud](https://blog.bitbucket.org/2013/05/14/edit-your-code-in-the-cloud-with-bitbucket/). Through the hackathons, the companies and venture capitalists were able to make informed decisions on areas they could invest in for innovation as well as funding.

##Application of Domain.com Coupons in hackathons

The success of any online presence is hedged on having a great domain. Using the Domain.com coupons, MLH student hackers are able to get .com, .net, or .org domains for free. These coupons facilitate the process of domain acquisition by making it easier, faster and more affordable.

At [Domain.com](http://www.domain.com/) there are all the major Top Level Domains (TLDs), as well as more than 25 Country Code Top Level Domains (ccTLDs). 

Domain.com discount code 18COOL, gives everyone, regardless of whether they are an MLH student hacker or not, an 18% discount when registering any domain name or buying hosting at Domain.com.
